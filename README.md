oc new-app fabric8/s2i-java~https://markklim@bitbucket.org/markklim/gp-hw --name gp-gradle-test

ROUTE:
http://gp-gradle-test-route-gpproject.192.168.64.7.nip.io

EXAMPLE:
http://gp-gradle-test-route-gpproject.192.168.64.7.nip.io/service/ping

mvn spring-boot:run
localhost:8080/actuator/prometheus
