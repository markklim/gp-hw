package com.gp.hw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ImportResource;

//@ImportResource({"classpath:camel-context.xml"})
@SpringBootApplication
@EnableConfigurationProperties
public class Application {
    public static void main(String... args) {
        SpringApplication.run(Application.class, args);
    }
}
