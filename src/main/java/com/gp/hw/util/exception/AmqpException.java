package com.gp.hw.util.exception;

public class AmqpException extends RuntimeException {
    public AmqpException(Throwable cause) {
        super(cause);
    }
}
