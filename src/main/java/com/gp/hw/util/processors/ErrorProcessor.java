package com.gp.hw.util.processors;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;


@Slf4j
//TODO: Get rid of it
/**
 * Процессор-перехватчик и обёрточник всех возникших исключений, проброшенных на уровень приложения.
 * Необходим для внятного и наглядного представления исключения в REST-ответе в формате json.
 */
public class ErrorProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        log.info("Error");
    }
}
