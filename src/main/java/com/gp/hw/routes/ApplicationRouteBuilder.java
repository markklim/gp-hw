package com.gp.hw.routes;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import static org.apache.camel.LoggingLevel.*;

@Component
public class ApplicationRouteBuilder extends RouteBuilder {
    private static final String APPLICATION_HEADER = "ApplicationName";

    final String readinessCheck = "direct:readinessCheck";
    final String directAmqReadiness = "direct:AMQreadiness";
    final String amqpAmqReadiness = "amqp:queue:amq_readiness";

    final String createUserReport = "direct:createUserReport";
    final String amqpReportQueue = "amqp:queue:report_queue";

    final String finish = "direct:finish";

    @Override
    public void configure() {
        /*onException(Exception.class)
                .handled(true)
                .log(ERROR, String.format("${in.header.%s}: ${exception.message} of ${in.header.CamelFileName}", APPLICATION_HEADER));

        restConfiguration()
                .contextPath("rest")
                .port(8080)
                .enableCORS(true)
                .apiProperty("api.version", "1.0")
                .corsHeaderProperty("Access-Control-Allow-Headers", "*")
                .corsHeaderProperty("Access-Control-Allow-Origin", "*");

        createReadinessPipeline();
        createUserReportPipeline();*/
    }

    /*void createReadinessPipeline() {
        rest().id("readiness")
                .get("status/readiness")
                .description("Check if service is ready to process business logic")
                .to(readinessCheck);

        from(readinessCheck)
                .routeId("readinessCheck")
                .log("readinessCheck")
                .setBody().simple("OK")
                .doTry()
                .to(amqpAmqReadiness)
                .to(directAmqReadiness)
                .doCatch(java.lang.Exception.class)
                .setHeader("CamelHttpResponseCode")
                .simple("501")
                .to(finish)
                .end();

        from(directAmqReadiness, amqpAmqReadiness)
                .routeId("AMQreadiness")
                .choice()
                .when((Exchange exchange) -> exchange.getIn().getBody(String.class).equals("OK"))
                .log("ready")
                .to("direct:finish")
                .when((Exchange exchange) -> !exchange.getIn().getBody(String.class).equals("OK"))
                .log("not ready")
                .setHeader("CamelHttpResponseCode")
                .simple("501")
                .to(finish)
                .endChoice();

        from(finish)
                .routeId("finish")
                .setHeader("Content-type")
                .simple("Application/json")
                .log(DEBUG, "Application running!")
                .to("log:finish");
    }

    void createUserReportPipeline() {
        rest().id("userReport")
                .post("report/users")
                .description("Get users report")
                .to(createUserReport);

        from(createUserReport)
                .routeId("AMQPcreateUserReport")
                .setBody().simple("report")
                .removeHeader("Authorization")
                .convertBodyTo(String.class)
                .to(amqpReportQueue);

        from(amqpReportQueue)
                .routeId("AMQcreateUserReportResponse")
                .log(INFO, "Report OK!");
    }*/
}
