package com.gp.hw.configuration.amqp;

import com.gp.hw.util.exception.AmqpException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.net.ssl.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;

@Configuration
@Slf4j
public class AmqpSslConfiguration {
    private AmqpProperties properties;

    /*@Autowired
    public AmqpSslConfiguration(AmqpProperties properties) {
        this.properties = properties;
    }

    @Bean
    SSLContext sslContext () {
        try {
            SSLContext sslContext = SSLContext.getInstance(properties.tlsVersion);
            sslContext.init(getKeyManagers(), getTrustManagers(), null);
            sslContext.createSSLEngine();
            return sslContext;
        } catch (GeneralSecurityException e) {
            throw new AmqpException(e);
        }
    }*/

    private KeyManager[] getKeyManagers() throws GeneralSecurityException {
        char[] password = properties.key.server.keyStorePassword.toCharArray();

        KeyManagerFactory kmFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmFactory.init(getStore(properties.key.server.keyStorePath, password), password);
        return kmFactory.getKeyManagers();
    }

    private TrustManager[] getTrustManagers() throws GeneralSecurityException {
        TrustManagerFactory tmFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmFactory.init(getStore(properties.key.server.trustStorePath, null));
        return tmFactory.getTrustManagers();
    }

    private KeyStore getStore(String storePath, char[] password) throws GeneralSecurityException {
        try (FileInputStream inputStream = new FileInputStream(storePath)) {
            KeyStore store = KeyStore.getInstance(KeyStore.getDefaultType());
            store.load(inputStream, password);
            return store;
        } catch (IOException e) {
            throw new AmqpException(e);
        }
    }
}

