package com.gp.hw.configuration.amqp;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
@ConfigurationProperties(prefix = "amqp")
class AmqpProperties {
    String url;
    String username;
    String password;

    String tlsVersion = "TLSv1.2";
    Integer maxConnections = 5;

    Key key;

    @Data
    static class Key {
        KeyProperties server;
    }

    @Data
    static class KeyProperties {
        String keyStorePath;
        String keyStorePassword;

        String trustStorePath;
    }
}
